<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PHP basic practice</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cover.css" rel="stylesheet">
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">PHP</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="#">Home</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
            <h1 class="cover-heading">Welcome to PHP!</h1>
            <p class="lead">PHP is the best language, d'accord?</p>

      <form class="form-signin col-md-12" method=POST action=index.php>
        <label for="Username" class="sr-only">Username</label>
        <input type="code" id="code" name="code" class="form-control" placeholder="PHP > " required autofocus>
	<br><br>
       <button class="btn btn-lg btn-default lead" type="submit">RUN</button>
     </form>
	
	</div>
	<div class="col-md-12">
	<br><hr><hr><br>
	<p class="lead"> 
	<?php
	    if(isset($_REQUEST['code'])){
		    $code = "<?php ". $_REQUEST['code'] . "?>";
		    file_put_contents("tmp.php",$code);
		    echo "OUTCOME: \n";
		    include "tmp.php";
	    }
	?>
	</p>
          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>PHP learning project <a href="http://haozi.bit.edu.cn">Here</a>, by <a href="https://facebook.com/Hence Zhang">@Hence</a>.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

