<?php
  if (isset($_GET['path'])) {
    $content = file_get_contents($_GET['path']);
  }
 if (!isset($_GET['include'])){
    header("Location: index.php?include=0");
}
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">
      <h3>ASCII Art Viewer</h3>
      <form method="GET" target="/index.php">
        <div class="form-group">
          <label for="path">ASCII Art</label>
          <select name="path" class="form-control" id="path">
            <option value="cats.txt">Cat</option>
            <option value="dog.txt">Dog</option>
            <option value="zebra.txt">Zebra</option>
          </select>
	    <input name="include" value="0" type="hidden" />
        </div>

        <button type="submit" class="btn btn-primary">View</button>
      </form>

      <?php if (isset($content)) { ?>
      <br />
      <pre><?php if($_GET['include']){include($_GET['path']);}else{echo $content;} ?></pre>
      <?php } ?>
    </div>
  </body>
</html>
