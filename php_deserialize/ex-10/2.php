<?php  
show_source(__FILE__);
   
class TestClass  
{  
   
    public $variable = 'This is a string';  
    public $a = 'a';   
   
    public function PrintVariable()  
    {  
        echo $this->variable . '<br />';  
    }  
    
    public function __wakeup()
    {
	echo '__wakeup <br />';
    }
    
   
    public function __construct()  
    {  
        echo '__construct <br />';  
    }  
   
   
    public function __destruct()  
    {  
        echo '__destruct <br />';  
    }  

}  
   
$object = new TestClass();  
   
$object->PrintVariable();  
   
var_dump(serialize($object)."<br />");

?> 
