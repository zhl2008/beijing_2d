<?php  
show_source(__FILE__);
   
class TestClass  
{  
   
    public $variable = 'This is a string';  
   
   
    public function PrintVariable()  
    {  
        echo $this->variable . '<br />';  
    }  
    
    public function __wakeup()
    {
	echo '__wakeup <br />';
    } 
    
    public function __sleep()
    {
	echo '__sleep <br />';
    }	    
   
    public function __construct()  
    {  
        echo '__construct <br />';  
    }  
   
   
    public function __destruct()  
    {  
        echo '__destruct <br />';  
    }  

}  
   
$object = unserialize('O:9:"TestClass":1:{s:8:"variable";s:6:"Hacked";}');

$object -> PrintVariable();
   

?> 
