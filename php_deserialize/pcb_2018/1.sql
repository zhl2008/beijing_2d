DROP DATABASE IF EXISTS t2;
CREATE database t2;
USE t2;
CREATE TABLE users
                (
                id int(3) NOT NULL AUTO_INCREMENT,
                username varchar(50) NOT NULL,
                password varchar(50) NOT NULL,
		filepath  varchar(200),
                PRIMARY KEY (id)
                );

INSERT INTO t2.users (id, username, password) VALUES ('1', 'admin','admin');
