#include <iostream>
using namespace std;
const int MAX_VERT = 10;
struct Graphic{
	int vertNum;
	int VertValue[MAX_VERT];
	int AdjMatrix[MAX_VERT][MAX_VERT];
	bool visited[MAX_VERT];
};

void initGraphic(Graphic &G, int n){
	G.vertNum = n;
	int i, j;
	for(i = 0; i < G.vertNum; i++){
		for(j = 0; j < G.vertNum; j++)
			cin>>G.AdjMatrix[i][j];
	}

	for(i = 0; i< G.vertNum; i++){
		G.VertValue[i] = i;
		G.visited[i] = false;
	}
}
void TopoSort(Graphic G){
	int i, j, k, m;
	
	for(m = 0; m <G.vertNum; m++){

		for(i = 0; i < G.vertNum; i++){

			for(j = 0; j < G.vertNum; j++){

				if(G.AdjMatrix[j][i])
					break;
		}
		if(j == G.vertNum && !G.visited[i])
			break;
		}
	cout<<i<<" ";
	for(k = 0; k <G.vertNum; k++)
		G.AdjMatrix[i][k] = 0;
	G.visited[i] = true;
	}
	cout<<endl;
}


int main()
{
	int t, n, i;
	Graphic G[2];
	cin>>t;
	for(i = 0; i < t; i++){
		cin>>n;
		initGraphic(G[i], n);
		TopoSort(G[i]);
	}
	
	return 0;
}

