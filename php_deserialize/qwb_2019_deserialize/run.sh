#!/bin/sh
cp /var/www/html/000-default.conf /etc/apache2/sites-enabled/000-default.conf
a2enmod rewrite
service apache2 stop
service apache2 start
chown mysql:mysql  -R /var/lib/mysql
service mysql start
cd /var/www/html
mysql -uroot -proot < 1.sql
/bin/bash
